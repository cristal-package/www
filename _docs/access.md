---
title: Access
permalink: /docs/access/
---

CRISTAL is distributed through:

  * [NEA DataBank Computer program service](http://oecd-nea.org/tools/cpsrequest/start/NEA-1903%2501) for NEA Data Bank participating countries,
  * [Direct access](mailto:cristal@irsn.fr) for french partners.
