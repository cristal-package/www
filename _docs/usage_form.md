---
title: "CRISTAL users feedback"
permalink: /docs/feedback/
---

<form id="usageform" method="POST">
    <center>
        <label for="name">
            (optional) :
        </label>
        <input type="text" name="name" placeholder="Your name">
        &nbsp;
        <input type="email" name="_replyto" placeholder="Your email">
    </center>

    <hr />

    <label for="version">
        Which CRISTAL version do you use ?&nbsp;
    </label>
    <select name="version">
        <option value="?" selected="" disabled="">- ? -</option>
        <option value="V2.0">V2.0</option>
        <option value="V2.0.1">V2.0.1</option>
        <option value="V2.0.2">V2.0.2</option>
        <option value="V2.0.3">V2.0.3</option>
    </select>

    <br />
    <br />
    <label for="match">
        Adequacy of CRISTAL V2 tools and codes to your calculation needs :&nbsp;
        <br /><i>(1: poor to 5: very good)</i>
    </label>
    1/5
    <input type="radio" name="match-tools" value="1/5">
    <input type="radio" name="match-tools" value="2/5">
    <input type="radio" name="match-tools" value="3/5">
    <input type="radio" name="match-tools" value="4/5">
    <input type="radio" name="match-tools" value="5/5">
    5/5

    <br />

    <label for="tools-comment">
        Which CRISTAL V2 feature require evolutions (trainning, use, setup, validation, verification, ...) ?
    </label>
    <br /><textarea rows="3" cols="50" name="tools-comment" placeholder="Your comments"></textarea>

    <br />

    <label for="channel">
        How do you contact CRISTAL project team ?
    </label>
    <br /><input type="checkbox" name="channel" value="direct"> Direct contact (email, phone, ...)
    <br /><input type="checkbox" name="channel" value="tracker"> Tracker on CRISTAL forge
    <br /><input type="checkbox" name="channel" value="groups"> Project working groups

    <br />
    <br />
    <label for="match-training">
        Adequacy of CRISTAL's trainings to users needs :&nbsp;
        <br /><i>(1: poor to 5: very good)</i>
    </label>
    1/5
    <input type="radio" name="match-training" value="1/5">
    <input type="radio" name="match-training" value="2/5">
    <input type="radio" name="match-training" value="3/5">
    <input type="radio" name="match-training" value="4/5">
    <input type="radio" name="match-training" value="5/5">
    5/5

    <br />

    <label for="training-comment">
        What subjects of the CRISTAL's training are missing or need to be further developed?
    </label>
    <br /><textarea rows="3" cols="50" name="training-comment" placeholder="Your comments"></textarea>

    <br />

    <label for="match-install">
        Adequacy of trainings material to trainees' needs (DVD, USB) :&nbsp;
        <br /><i>(1: poor to 5: very good)</i>
    </label>
    1/5
    <input type="radio" name="match-install" value="1/5">
    <input type="radio" name="match-install" value="2/5">
    <input type="radio" name="match-install" value="3/5">
    <input type="radio" name="match-install" value="4/5">
    <input type="radio" name="match-install" value="5/5">
    5/5

    <br />

    <label for="install-comment">
        What difficulties did you encounter with CRISTAL V2 setup ?
    </label>
    <br /><textarea rows="3" cols="50" name="install-comment" placeholder="Your comments"></textarea>

    <hr />

    <b>How often do you use the following CRISTAL features :</b>

    <table>
        <thead>
            <tr>
                <td> </td>
                <td> Rarely </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> Regulary </td>
            </tr>
        </thead>

        <thead>
            <tr>
                <td> <b>LATEC </b></td>
            </tr>
        </thead>
        <tr>
            <td> Parameterization</td>
            <td> <input type="radio" name="usage-parameters" value="1/5"></td>
            <td> <input type="radio" name="usage-parameters" value="2/5"></td>
            <td> <input type="radio" name="usage-parameters" value="3/5"></td>
            <td> <input type="radio" name="usage-parameters" value="4/5"></td>
            <td> <input type="radio" name="usage-parameters" value="5/5"></td>
        </tr>

        <tr>
            <td> Formula</td>
            <td> <input type="radio" name="usage-formulas" value="1/5"></td>
            <td> <input type="radio" name="usage-formulas" value="2/5"></td>
            <td> <input type="radio" name="usage-formulas" value="3/5"></td>
            <td> <input type="radio" name="usage-formulas" value="4/5"></td>
            <td> <input type="radio" name="usage-formulas" value="5/5"></td>
        </tr>

        <tr>
            <td> Density laws </td>
            <td> <input type="radio" name="usage-lawdil" value="1/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="2/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="3/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="4/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="5/5"></td>
        </tr>

        <tr>
            <td> 3D view </td>
            <td> <input type="radio" name="usage-visu3D" value="1/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="2/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="3/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="4/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="5/5"></td>
        </tr>

        <tr>
            <td> CRISTAL V1 import </td>
            <td> <input type="radio" name="usage-import" value="1/5"></td>
            <td> <input type="radio" name="usage-import" value="2/5"></td>
            <td> <input type="radio" name="usage-import" value="3/5"></td>
            <td> <input type="radio" name="usage-import" value="4/5"></td>
            <td> <input type="radio" name="usage-import" value="5/5"></td>
        </tr>

        <tr>
            <td> Preview of input </td>
            <td> <input type="radio" name="usage-visujdd" value="1/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="2/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="3/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="4/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="5/5"></td>
        </tr>

        <tr>
            <td> Depletion calculation import (Cesar/Darwin/...)</td>
            <td> <input type="radio" name="usage-importevol" value="1/5"></td>
            <td> <input type="radio" name="usage-importevol" value="2/5"></td>
            <td> <input type="radio" name="usage-importevol" value="3/5"></td>
            <td> <input type="radio" name="usage-importevol" value="4/5"></td>
            <td> <input type="radio" name="usage-importevol" value="5/5"></td>
        </tr>


        <thead>
            <tr>
                <td> <b>Calculation routes</b></td>
            </tr>
        </thead>
        <tr>
            <td>Pij-MonteCarlo (APOLLO2.8-MORET 5B) </td>
            <td> <input type="radio" name="usage-pijmc" value="1/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="2/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="3/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="4/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="5/5"></td>
        </tr>

        <tr>
            <td>PijMultiCell-MonteCarlo (APOLLO2.8-MORET 5B) </td>
            <td> <input type="radio" name="usage-pijxcmc" value="1/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="2/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="3/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="4/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Keff 1D (APOLLO2.8) </td>
            <td> <input type="radio" name="usage-sn1D" value="1/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="2/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="3/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="4/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Keff 2D (APOLLO2.8) </td>
            <td> <input type="radio" name="usage-sn2D" value="1/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="2/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="3/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="4/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Normes (APOLLO2.8) </td>
            <td> <input type="radio" name="usage-snnormes" value="1/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="2/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="3/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="4/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="5/5"></td>
        </tr>

        <tr>
            <td> Point-wise MonteCarlo (TRIPOLI4.8) </td>
            <td> <input type="radio" name="usage-mc" value="1/5"></td>
            <td> <input type="radio" name="usage-mc" value="2/5"></td>
            <td> <input type="radio" name="usage-mc" value="3/5"></td>
            <td> <input type="radio" name="usage-mc" value="4/5"></td>
            <td> <input type="radio" name="usage-mc" value="5/5"></td>
        </tr>


    </table>

    <hr />

   <b> Ease of use of the following features <i>(leave blank when no advice)</i>  :</b>

    <table>
        <thead>
            <tr>
                <td> </td>
                <td> Poor </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> Very good </td>
            </tr>
        </thead>

        <thead>
            <tr>
                <td> <b>LATEC</b> </td>
            </tr>
        </thead>
        <tr>
            <td> Parameterization</td>
            <td> <input type="radio" name="easy-parameters" value="1/5"></td>
            <td> <input type="radio" name="easy-parameters" value="2/5"></td>
            <td> <input type="radio" name="easy-parameters" value="3/5"></td>
            <td> <input type="radio" name="easy-parameters" value="4/5"></td>
            <td> <input type="radio" name="easy-parameters" value="5/5"></td>
        </tr>

        <tr>
            <td> Formula</td>
            <td> <input type="radio" name="easy-formulas" value="1/5"></td>
            <td> <input type="radio" name="easy-formulas" value="2/5"></td>
            <td> <input type="radio" name="easy-formulas" value="3/5"></td>
            <td> <input type="radio" name="easy-formulas" value="4/5"></td>
            <td> <input type="radio" name="easy-formulas" value="5/5"></td>
        </tr>

        <tr>
            <td> Density laws </td>
            <td> <input type="radio" name="easy-lawdil" value="1/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="2/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="3/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="4/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="5/5"></td>
        </tr>

        <tr>
            <td> 3D view </td>
            <td> <input type="radio" name="easy-visu3D" value="1/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="2/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="3/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="4/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="5/5"></td>
        </tr>

        <tr>
            <td> CRISTAL V1 import </td>
            <td> <input type="radio" name="easy-import" value="1/5"></td>
            <td> <input type="radio" name="easy-import" value="2/5"></td>
            <td> <input type="radio" name="easy-import" value="3/5"></td>
            <td> <input type="radio" name="easy-import" value="4/5"></td>
            <td> <input type="radio" name="easy-import" value="5/5"></td>
        </tr>

        <tr>
            <td> Preview of input </td>
            <td> <input type="radio" name="easy-visujdd" value="1/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="2/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="3/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="4/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="5/5"></td>
        </tr>

        <tr>
            <td> Depletion calculation import (Cesar/Darwin/...)</td>
            <td> <input type="radio" name="easy-importevol" value="1/5"></td>
            <td> <input type="radio" name="easy-importevol" value="2/5"></td>
            <td> <input type="radio" name="easy-importevol" value="3/5"></td>
            <td> <input type="radio" name="easy-importevol" value="4/5"></td>
            <td> <input type="radio" name="easy-importevol" value="5/5"></td>
        </tr>


        <thead>
            <tr>
                <td> <b>Calculation routes</b></td>
            </tr>
        </thead>
        <tr>
            <td> Pij-MonteCarlo (APOLLO2.8-MORET 5B) </td>
            <td> <input type="radio" name="easy-pijmc" value="1/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="2/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="3/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="4/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="5/5"></td>
        </tr>

        <tr>
            <td>PijMultiCellule-Moret(APOLLO2.8-MORET 5B) </td>
            <td> <input type="radio" name="easy-pijxcmc" value="1/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="2/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="3/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="4/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Keff 1D (APOLLO2.8) </td>
            <td> <input type="radio" name="easy-sn1D" value="1/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="2/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="3/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="4/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Keff 2D (APOLLO2.8) </td>
            <td> <input type="radio" name="easy-sn2D" value="1/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="2/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="3/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="4/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="5/5"></td>
        </tr>

        <tr>
            <td>Sn Normes (APOLLO2.8) </td>
            <td> <input type="radio" name="easy-snnormes" value="1/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="2/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="3/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="4/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="5/5"></td>
        </tr>

        <tr>
            <td> Point-wise MonteCarlo (TRIPOLI4.8) </td>
            <td> <input type="radio" name="easy-mc" value="1/5"></td>
            <td> <input type="radio" name="easy-mc" value="2/5"></td>
            <td> <input type="radio" name="easy-mc" value="3/5"></td>
            <td> <input type="radio" name="easy-mc" value="4/5"></td>
            <td> <input type="radio" name="easy-mc" value="5/5"></td>
        </tr>

    </table>


    <input type="hidden" name="_subject" value="Usage" />
    <input type="text" name="_gotcha" style="display:none" />
    <br />
    <input type="submit" value="Send">
    <input type="hidden" name="_next" value="https://www.cristal-package.org/doc/thanks_usage" />

</form>
<script>
    var usageform = document.getElementById('usageform');
    usageform.setAttribute('action', '//formspree.io/' + 'cristal' + '@' + 'irsn' + '.' + 'fr');
</script>
