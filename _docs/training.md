---
title: Training
permalink: /docs/training/
---


## Course

Main training courses are available through [ENSTTI](http://enstti.eu/) training institute.

__Title__: Training Program for CRISTAL users

__Location__: Paris, France

__Duration__: 4-5 days

__Description__: The CRISTAL calculation tool for the field of nuclear criticality safety has a data library and a new-generation user interface (LATEC). It is designed for safety studies, in particular in the case of requirements like MOX fuel, high burn-up fuel, etc.


## Workshop

__Title__: Second level criticality modelling with CRISTAL package: enhancing criticality safety assessments for industrial applications

__Location__: Depends on inviting event

__Duration__: 2h (short version) / 1 day (extended version)

__Description__: Design annular tank for U solution storage using CRISTAL V2 package
