---
title: "APOLLO2"
permalink: /docs/apollo/
---
The APOLLO2 spectral transport code, developed at the Commissariat à l’Energie Atomique et aux Energies Alternatives (CEA) with financial support from Framatome and EDF, is widely used for cross section generation and direct transport calculations, including a large range of applications in reactor physics, criticality safety studies and fuel cycle analysis. Its utilization covers R&D analysis, interpretation of reactor experiments and industrial applications. The code is an integrated component for multigroup cross section generation of other CEA and third-party industrial software packages and it is also used for benchmarking and educational activities.

APOLLO2 is used for routine as well as for reference transport calculations. It is the main tool used for the design and analysis of all the CEA reactors and experimental
facilities. The code has been and is being intensively used for the conception of the new CEA experimental reactor JHR. In particular, the unique capability of APOLLO2
to compute curved fuel plates shaped along circle involutes has been used for the core cycle analysis of the High Flux Reactor of the Laue Langevin Institute. APOLLO2
provides self-shielded cross sections and neutron spectra for the DARWIN2 code system for fuel cycle studies and it is also an integrated component of the French Institute
for Radiation Protection and Nuclear Safety (IRSN) criticality package CRISTAL for safety assessments, where the self-shielding procedures and the discrete ordinates and collision probabilities solvers of APOLLO2 are put to use to generate homogenized cross sections for multigroup Monte Carlo calculations. 

![Self shielding]({{ site.baseurl }}/docs/AP2_calc.png)

Schematics of a Self-shielding Calculation:

  - A Slowing-down resonant model is invoked to determine the absorption reaction rates,
  - The background cross section is determined so that the reaction rates are obtained with the same resonant model in an equivalent infinite and homogeneous medium with background cross section,
  - The background cross section is used to compute on the fly or to retrieve from tabulated values “exact” reaction rates in the infinite and homogeneous medium,
  - Multigroup self-shielded cross sections are determined so that a multigroup calculation with these cross sections reproduces the reactions rates.
