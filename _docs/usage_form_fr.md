---
title: "Retour d'expérience"
permalink: /docs/rex/
---

<form id="usageform" method="POST">
    <center>
        <label for="name">
            (optionel) :
        </label>
        <input type="text" name="name" placeholder="Your name">
        &nbsp;
        <input type="email" name="_replyto" placeholder="Your email">
    </center>

    <hr />

    <label for="version">
        Quelle version de CRISTAL utilisez vous ?&nbsp;
    </label>
    <select name="version">
        <option value="?" selected="" disabled="">- ? -</option>
        <option value="V2.0">V2.0</option>
        <option value="V2.0.1">V2.0.1</option>
        <option value="V2.0.2">V2.0.2</option>
        <option value="V2.0.3">V2.0.3</option>
    </select>

    <br />

    <label for="match">
        Adéquation des outils du formulaire CRISTAL V2 à vos besoins de modélisation :&nbsp;
    </label>
    1/5
    <input type="radio" name="match-tools" value="1/5">
    <input type="radio" name="match-tools" value="2/5">
    <input type="radio" name="match-tools" value="3/5">
    <input type="radio" name="match-tools" value="4/5">
    <input type="radio" name="match-tools" value="5/5">
    5/5

    <br />

    <label for="tools-comment">
        Sur quelles fonctionnalités identifiez vous des évolutions nécessaires de CRISTAL V2
        <br />(formation, utilisation, installation, qualification, interface utilisateur, ...) ?
    </label>
    <br /><textarea rows="3" cols="50" name="tools-comment" placeholder="Your comments"></textarea>

    <br />

    <label for="channel">
        Quels moyens de communication utilisez vous avec le projet CRISTAL ?
    </label>
    <br /><input type="checkbox" name="channel" value="direct">contacts directs (mail, téléphone, ...)
    <br /><input type="checkbox" name="channel" value="tracker">tracker de la forge CRISTAL
    <br /><input type="checkbox" name="channel" value="groups">groupes d'échanges du projet

    <br />

    <label for="match-training">
        Adaptation des formations dispensées pour l'utilisation du formulaire CRISTAL V2 :&nbsp;
    </label>
    1/5
    <input type="radio" name="match-training" value="1/5">
    <input type="radio" name="match-training" value="2/5">
    <input type="radio" name="match-training" value="3/5">
    <input type="radio" name="match-training" value="4/5">
    <input type="radio" name="match-training" value="5/5">
    5/5

    <br />

    <label for="training-comment">
        Quels éléments de formations vous semblent manquants/à approfondir ?
    </label>
    <br /><textarea rows="3" cols="50" name="training-comment" placeholder="Your comments"></textarea>

    <br />

    <label for="match-install">
        Adaptation des supports matériels d'installation du formulaire CRISTAL V2 (DVD, clés USB) :&nbsp;
    </label>
    1/5
    <input type="radio" name="match-install" value="1/5">
    <input type="radio" name="match-install" value="2/5">
    <input type="radio" name="match-install" value="3/5">
    <input type="radio" name="match-install" value="4/5">
    <input type="radio" name="match-install" value="5/5">
    5/5

    <br />

    <label for="install-comment">
        Quelles difficultés avez vous rencontré pour cette installation ?
    </label>
    <br /><textarea rows="3" cols="50" name="install-comment" placeholder="Your comments"></textarea>

    <hr />

    <b>Evaluez votre fréquence d'utilisation des fonctionnalités suivantes :</b>

    <table>
        <thead>
            <tr>
                <td> </td>
                <td> Rare </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> Fréquente </td>
            </tr>
        </thead>

        <thead>
            <tr>
                <td> <b>LATEC </b></td>
            </tr>
        </thead>
        <tr>
            <td> paramétrage</td>
            <td> <input type="radio" name="usage-parameters" value="1/5"></td>
            <td> <input type="radio" name="usage-parameters" value="2/5"></td>
            <td> <input type="radio" name="usage-parameters" value="3/5"></td>
            <td> <input type="radio" name="usage-parameters" value="4/5"></td>
            <td> <input type="radio" name="usage-parameters" value="5/5"></td>
        </tr>

        <tr>
            <td> formules</td>
            <td> <input type="radio" name="usage-formulas" value="1/5"></td>
            <td> <input type="radio" name="usage-formulas" value="2/5"></td>
            <td> <input type="radio" name="usage-formulas" value="3/5"></td>
            <td> <input type="radio" name="usage-formulas" value="4/5"></td>
            <td> <input type="radio" name="usage-formulas" value="5/5"></td>
        </tr>

        <tr>
            <td> lois de dilution </td>
            <td> <input type="radio" name="usage-lawdil" value="1/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="2/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="3/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="4/5"></td>
            <td> <input type="radio" name="usage-lawdil" value="5/5"></td>
        </tr>

        <tr>
            <td> visualisation 3D </td>
            <td> <input type="radio" name="usage-visu3D" value="1/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="2/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="3/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="4/5"></td>
            <td> <input type="radio" name="usage-visu3D" value="5/5"></td>
        </tr>

        <tr>
            <td> importation CRISTAL V1 </td>
            <td> <input type="radio" name="usage-import" value="1/5"></td>
            <td> <input type="radio" name="usage-import" value="2/5"></td>
            <td> <input type="radio" name="usage-import" value="3/5"></td>
            <td> <input type="radio" name="usage-import" value="4/5"></td>
            <td> <input type="radio" name="usage-import" value="5/5"></td>
        </tr>

        <tr>
            <td> visualisation du jdd </td>
            <td> <input type="radio" name="usage-visujdd" value="1/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="2/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="3/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="4/5"></td>
            <td> <input type="radio" name="usage-visujdd" value="5/5"></td>
        </tr>

        <tr>
            <td> importation d'évolution (Cesar/Darwin/...)</td>
            <td> <input type="radio" name="usage-importevol" value="1/5"></td>
            <td> <input type="radio" name="usage-importevol" value="2/5"></td>
            <td> <input type="radio" name="usage-importevol" value="3/5"></td>
            <td> <input type="radio" name="usage-importevol" value="4/5"></td>
            <td> <input type="radio" name="usage-importevol" value="5/5"></td>
        </tr>


        <thead>
            <tr>
                <td> <b>Schémas</b></td>
            </tr>
        </thead>
        <tr>
            <td> calcul Pij-MonteCarlo (Apollo-Moret) </td>
            <td> <input type="radio" name="usage-pijmc" value="1/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="2/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="3/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="4/5"></td>
            <td> <input type="radio" name="usage-pijmc" value="5/5"></td>
        </tr>

        <tr>
            <td>calcul PijMultiCellule-Moret(Apollo-Moret) </td>
            <td> <input type="radio" name="usage-pijxcmc" value="1/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="2/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="3/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="4/5"></td>
            <td> <input type="radio" name="usage-pijxcmc" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Keff 1D (Apollo) </td>
            <td> <input type="radio" name="usage-sn1D" value="1/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="2/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="3/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="4/5"></td>
            <td> <input type="radio" name="usage-sn1D" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Keff 2D(Apollo) </td>
            <td> <input type="radio" name="usage-sn2D" value="1/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="2/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="3/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="4/5"></td>
            <td> <input type="radio" name="usage-sn2D" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Normes(Apollo) </td>
            <td> <input type="radio" name="usage-snnormes" value="1/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="2/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="3/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="4/5"></td>
            <td> <input type="radio" name="usage-snnormes" value="5/5"></td>
        </tr>

        <tr>
            <td> calculs MonteCarlo ponctuels(Tripoli) </td>
            <td> <input type="radio" name="usage-mc" value="1/5"></td>
            <td> <input type="radio" name="usage-mc" value="2/5"></td>
            <td> <input type="radio" name="usage-mc" value="3/5"></td>
            <td> <input type="radio" name="usage-mc" value="4/5"></td>
            <td> <input type="radio" name="usage-mc" value="5/5"></td>
        </tr>


        <thead>
            <tr>
                <td> <b>Outils associés</b></td>
            </tr>
        </thead>
        <tr>
            <td> MACSENS </td>
            <td> <input type="radio" name="usage-macsens" value="1/5"></td>
            <td> <input type="radio" name="usage-macsens" value="2/5"></td>
            <td> <input type="radio" name="usage-macsens" value="3/5"></td>
            <td> <input type="radio" name="usage-macsens" value="4/5"></td>
            <td> <input type="radio" name="usage-macsens" value="5/5"></td>
        </tr>

        <tr>
            <td> FOURMI </td>
            <td> <input type="radio" name="usage-fourmi" value="1/5"></td>
            <td> <input type="radio" name="usage-fourmi" value="2/5"></td>
            <td> <input type="radio" name="usage-fourmi" value="3/5"></td>
            <td> <input type="radio" name="usage-fourmi" value="4/5"></td>
            <td> <input type="radio" name="usage-fourmi" value="5/5"></td>
        </tr>

        <tr>
            <td> RIB </td>
            <td> <input type="radio" name="usage-rib" value="1/5"></td>
            <td> <input type="radio" name="usage-rib" value="2/5"></td>
            <td> <input type="radio" name="usage-rib" value="3/5"></td>
            <td> <input type="radio" name="usage-rib" value="4/5"></td>
            <td> <input type="radio" name="usage-rib" value="5/5"></td>
        </tr>

        <tr>
            <td> DIANE </td>
            <td> <input type="radio" name="usage-diane" value="1/5"></td>
            <td> <input type="radio" name="usage-diane" value="2/5"></td>
            <td> <input type="radio" name="usage-diane" value="3/5"></td>
            <td> <input type="radio" name="usage-diane" value="4/5"></td>
            <td> <input type="radio" name="usage-diane" value="5/5"></td>
        </tr>

        <tr>
            <td> PROMETHEE </td>
            <td> <input type="radio" name="usage-promethee" value="1/5"></td>
            <td> <input type="radio" name="usage-promethee" value="2/5"></td>
            <td> <input type="radio" name="usage-promethee" value="3/5"></td>
            <td> <input type="radio" name="usage-promethee" value="4/5"></td>
            <td> <input type="radio" name="usage-promethee" value="5/5"></td>
        </tr>

    </table>

    <hr />

    <b>Evaluez la facilité d'utilisation des fonctionnalités suivantes :
    <br/>(laisser vide si vous  n'avez pas d'avis)</b>

    <table>
        <thead>
            <tr>
                <td> </td>
                <td> Faible </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> Bonne </td>
            </tr>
        </thead>

        <thead>
            <tr>
                <td> <b>LATEC</b> </td>
            </tr>
        </thead>
        <tr>
            <td> paramétrage</td>
            <td> <input type="radio" name="easy-parameters" value="1/5"></td>
            <td> <input type="radio" name="easy-parameters" value="2/5"></td>
            <td> <input type="radio" name="easy-parameters" value="3/5"></td>
            <td> <input type="radio" name="easy-parameters" value="4/5"></td>
            <td> <input type="radio" name="easy-parameters" value="5/5"></td>
        </tr>

        <tr>
            <td> formules</td>
            <td> <input type="radio" name="easy-formulas" value="1/5"></td>
            <td> <input type="radio" name="easy-formulas" value="2/5"></td>
            <td> <input type="radio" name="easy-formulas" value="3/5"></td>
            <td> <input type="radio" name="easy-formulas" value="4/5"></td>
            <td> <input type="radio" name="easy-formulas" value="5/5"></td>
        </tr>

        <tr>
            <td> lois de dilution </td>
            <td> <input type="radio" name="easy-lawdil" value="1/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="2/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="3/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="4/5"></td>
            <td> <input type="radio" name="easy-lawdil" value="5/5"></td>
        </tr>

        <tr>
            <td> visualisation 3D </td>
            <td> <input type="radio" name="easy-visu3D" value="1/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="2/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="3/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="4/5"></td>
            <td> <input type="radio" name="easy-visu3D" value="5/5"></td>
        </tr>

        <tr>
            <td> importation CRISTAL V1 </td>
            <td> <input type="radio" name="easy-import" value="1/5"></td>
            <td> <input type="radio" name="easy-import" value="2/5"></td>
            <td> <input type="radio" name="easy-import" value="3/5"></td>
            <td> <input type="radio" name="easy-import" value="4/5"></td>
            <td> <input type="radio" name="easy-import" value="5/5"></td>
        </tr>

        <tr>
            <td> visualisation du jdd </td>
            <td> <input type="radio" name="easy-visujdd" value="1/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="2/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="3/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="4/5"></td>
            <td> <input type="radio" name="easy-visujdd" value="5/5"></td>
        </tr>

        <tr>
            <td> importation d'évolution (Cesar/Darwin/...)</td>
            <td> <input type="radio" name="easy-importevol" value="1/5"></td>
            <td> <input type="radio" name="easy-importevol" value="2/5"></td>
            <td> <input type="radio" name="easy-importevol" value="3/5"></td>
            <td> <input type="radio" name="easy-importevol" value="4/5"></td>
            <td> <input type="radio" name="easy-importevol" value="5/5"></td>
        </tr>


        <thead>
            <tr>
                <td> <b>Schémas</b></td>
            </tr>
        </thead>
        <tr>
            <td> calcul Pij-MonteCarlo (Apollo-Moret) </td>
            <td> <input type="radio" name="easy-pijmc" value="1/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="2/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="3/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="4/5"></td>
            <td> <input type="radio" name="easy-pijmc" value="5/5"></td>
        </tr>

        <tr>
            <td>calcul PijMultiCellule-Moret(Apollo-Moret) </td>
            <td> <input type="radio" name="easy-pijxcmc" value="1/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="2/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="3/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="4/5"></td>
            <td> <input type="radio" name="easy-pijxcmc" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Keff 1D (Apollo) </td>
            <td> <input type="radio" name="easy-sn1D" value="1/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="2/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="3/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="4/5"></td>
            <td> <input type="radio" name="easy-sn1D" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Keff 2D(Apollo) </td>
            <td> <input type="radio" name="easy-sn2D" value="1/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="2/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="3/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="4/5"></td>
            <td> <input type="radio" name="easy-sn2D" value="5/5"></td>
        </tr>

        <tr>
            <td>calculs Sn Normes(Apollo) </td>
            <td> <input type="radio" name="easy-snnormes" value="1/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="2/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="3/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="4/5"></td>
            <td> <input type="radio" name="easy-snnormes" value="5/5"></td>
        </tr>

        <tr>
            <td> calculs MonteCarlo ponctuels(Tripoli) </td>
            <td> <input type="radio" name="easy-mc" value="1/5"></td>
            <td> <input type="radio" name="easy-mc" value="2/5"></td>
            <td> <input type="radio" name="easy-mc" value="3/5"></td>
            <td> <input type="radio" name="easy-mc" value="4/5"></td>
            <td> <input type="radio" name="easy-mc" value="5/5"></td>
        </tr>

        <thead>
            <tr>
                <td> <b>Outils associés</b></td>
            </tr>
        </thead>
        <tr>
            <td> MACSENS </td>
            <td> <input type="radio" name="easy-macsens" value="1/5"></td>
            <td> <input type="radio" name="easy-macsens" value="2/5"></td>
            <td> <input type="radio" name="easy-macsens" value="3/5"></td>
            <td> <input type="radio" name="easy-macsens" value="4/5"></td>
            <td> <input type="radio" name="easy-macsens" value="5/5"></td>
        </tr>

        <tr>
            <td>FOURMI </td>
            <td> <input type="radio" name="easy-fourmi" value="1/5"></td>
            <td> <input type="radio" name="easy-fourmi" value="2/5"></td>
            <td> <input type="radio" name="easy-fourmi" value="3/5"></td>
            <td> <input type="radio" name="easy-fourmi" value="4/5"></td>
            <td> <input type="radio" name="easy-fourmi" value="5/5"></td>
        </tr>

        <tr>
            <td>RIB </td>
            <td> <input type="radio" name="easy-rib" value="1/5"></td>
            <td> <input type="radio" name="easy-rib" value="2/5"></td>
            <td> <input type="radio" name="easy-rib" value="3/5"></td>
            <td> <input type="radio" name="easy-rib" value="4/5"></td>
            <td> <input type="radio" name="easy-rib" value="5/5"></td>
        </tr>

        <tr>
            <td>DIANE </td>
            <td> <input type="radio" name="easy-diane" value="1/5"></td>
            <td> <input type="radio" name="easy-diane" value="2/5"></td>
            <td> <input type="radio" name="easy-diane" value="3/5"></td>
            <td> <input type="radio" name="easy-diane" value="4/5"></td>
            <td> <input type="radio" name="easy-diane" value="5/5"></td>
        </tr>

        <tr>
            <td>PROMETHEE </td>
            <td> <input type="radio" name="easy-promethee" value="1/5"></td>
            <td> <input type="radio" name="easy-promethee" value="2/5"></td>
            <td> <input type="radio" name="easy-promethee" value="3/5"></td>
            <td> <input type="radio" name="easy-promethee" value="4/5"></td>
            <td> <input type="radio" name="easy-promethee" value="5/5"></td>
        </tr>


    </table>


    <input type="hidden" name="_subject" value="Usage" />
    <input type="text" name="_gotcha" style="display:none" />
    <br />
    <input type="submit" value="Send">
    <input type="hidden" name="_next" value="https://www.cristal-package.org/doc/thanks_usage" />

</form>
<script>
    var usageform = document.getElementById('usageform');
    usageform.setAttribute('action', '//formspree.io/' + 'cristal' + '@' + 'irsn' + '.' + 'fr');
</script>
