---
title: "TRIPOLI-4(R)"
permalink: /docs/tripoli/
---

TRIPOLI-4 solves the linear Boltzmann equation for neutrons and photons, with the Monte Carlo method, in any 3D geometry. 
The code uses ENDF format continuous energy cross-sections, from various international evaluations including JEFF-3.1.1, ENDF/B-VII.0, JENDL4 and FENDL2.1.
Its official nuclear data library for applications, named CEAV5.1.1, is mainly based on the European evaluation JEFF-3.1.1 and is the only one delivered in this NEA package.
TRIPOLI-4 solves fixed source as well as eigenvalue problems. It has advanced variance reduction methods to address deep penetration issues.
Thanks to its robust and efficient parallelism capability, calculations are easily performed on multi-core single units, heterogeneous networks of workstations and massively parallel machines. 
Additional productivity tools, graphical as well as algorithmic, allow the user to efficiently set its input decks. With its large V&V data base, TRIPOLI-4 is used as a reference code for industrial purposes (fission/fusion), as well as a R&D and teaching tool, for radiation protection and shielding, core physics (without depletion in this NEA package), nuclear criticality-safety and nuclear instrumentation.


TRIPOLI-4 is a registered trademark of CEA.

## Nuclear data

Typically any ENDF-6 format evaluation may be used. In this NEA package, TRIPOLI-4 version 8.1 comes with the CEAV5.1.1 nuclear data library, mainly based on the JEFF-3.1.1 evaluation.

## Energy ranges

  * Neutron : 10E-11 to 20 MeV
  * Photon : 1 keV to 100 MeV

## Geometry

3-D surface based and combinatorial with network and network of networks.
The code comes with a 2D interactive display of geometry and material.

## Tallies

Volume, surface, point flux, current, reaction rate, deposited energy, keff, sub-critical multiplication factor, beta eff, dpa, pka, gaz production.

## Variance reduction

Exponential Transform, splitting/roulette, automated estimation of importance map.

## Perturbation

Density, concentration of isotopes, partial cross-sections.

## Verification & Validation

Based on SINBAD for radiation protection and shielding, ICSBEP for criticality-safety and CEA proprietary benchmarks for core physics.
 
## Data library distributed with TRIPOLI-4 v8.1

  * CEAV5.1.1 nuclear data library, mainly based on JEFF-3.1.1, with thermal cross sections and probability tables on the same temperature grid
  * Qfission: energy release during fission library
 