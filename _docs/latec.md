---
title: "LATEC"
permalink: /docs/latec/
---


LATEC is a workbench developed and used at IRSN for criticality studies.
It is one of the main components of the CRISTAL V2 criticality package. 


**Objectives**

The purpose of the LATEC workbench is to bring together all the tools needed for criticality safety studies in a single integrated workspace, from chemical media or geometry modelling to distributed calculations and post-processing of the results.  Built on the idea of providing the users with a unique development environment for all of the calculation software included within the CRISTAL V2 package, the interface allows for a unique modeling of a system, natively compatible with the codes TRIPOLI (CEA), APOLLO2 (CEA) and MORET 5 (IRSN).

**Modelling materials**

LATEC can be used to model both fissile and non-fissile materials and come equipped with ready to use predefined media for direct use in studies and criticality safety assessments. The modelling of complex solutions can be done through robust and validated dilution laws. Finally, within LATEC, users can import results from the codes CESAR (CEA) or VESTA (IRSN) for the use of depleted materials in assessments involving Credit Burn Up studies. 
 
![PeriodicTable]({{ site.baseurl }}/docs/imagesLATEC/PeriodicTable.png)


**Parametric studies and distributed calculations**

Users can use variables and complex formula in order to perform advanced parametric studies, involving thousands of calculation points. The calculation management engine automatically handles distributed calculation on distant servers for fast and efficient evaluations. Post-processing and visualization of the processed data can be achieved all within the interface.

![VariablePanel]({{ site.baseurl }}/docs/imagesLATEC/VariablePanel.png)

**Geometric modelling**

The modelling of geometries is achieved through a unique and user friendly interface allowing for the construction of 1D, 2D and 3D geometries, that can be directly used in all of the codes of the CRISTAL V2 package (APOLLO, TRIPOLI and MORET). Real time visualization of the geometry makes LATEC an efficient tool for the realization of complex systems. 

![GeomLATEC]({{ site.baseurl }}/docs/imagesLATEC/GeomLATEC.png)