---
title: "Support"
permalink: /docs/support/
---


## Ask a question

<center>
<form id="contactform" method="POST">
    <center>
    <input type="text" name="name" placeholder="Your name">
    &nbsp;
    <input type="email" name="_replyto" placeholder="Your email">
    <hr/>
    <input type="hidden" name="_subject" value="Contact" />
    <textarea rows="10" cols="50" name="message" placeholder="Your message"></textarea>
    <input type="text" name="_gotcha" style="display:none" />
    <br/>
    <input type="submit" value="Send">
    <input type="hidden" name="_next" value="https://www.cristal-package.org/doc/thanks_contact" />
    </center>
</form>
</center>
<script>
    var contactform =  document.getElementById('contactform');
    contactform.setAttribute('action', '//formspree.io/' + 'f/' + 'xknanagb');
</script>



## Submit new issue

<center>
<form id="issueform" method="POST">
    <center>
    <input type="text" name="name" placeholder="Your name">
    &nbsp;
    <input type="email" name="_replyto" placeholder="Your email">
    <hr/>
    <input type="hidden" name="_subject" value="Issue" />
    <textarea rows="10" cols="50" name="message" placeholder="Describe your issue"></textarea>
    <input type="text" name="_gotcha" style="display:none" />
    <br/>
    <input type="submit" value="Send">
    <input type="hidden" name="_next" value="https://www.cristal-package.org/doc/thanks_issue" />
    </center>
</form>
</center>
<script>
    var issueform =  document.getElementById('issueform');
    issueform.setAttribute('action', '//formspree.io/' + 'f/' + 'xknanagb');
</script>
